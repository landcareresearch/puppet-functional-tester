#!/bin/bash

echo "Importing Operating Systems to test"
source ../tests/os.bash

echo $TESTS

OUT="results.txt"

if [ -f $OUT ] ; then
  rm $OUT
fi

function cleanup {
    vagrant destroy -f
}

function prepare {
  if [ ! -d puppet ] ; then
    mkdir puppet
  fi
  
  if [ ! -d puppet/logs ] ; then
    mkdir -p puppet/logs
  fi
   
  if [ -f puppet/logs/puppet.log ] ; then
      rm puppet/logs/puppet.log
  fi
  
  if [ ! -d 'puppet/test' ] ; then
    mkdir -p 'puppet/test'
  fi
  
  # update dependencies
  scripts/run_g10k.bash
}

function test_os {
  echo "require './config/${1}'" > config/testos.rb
}

# Provisions the vm and runs the provisioner
function run_test {
    scripts/run.bash
    if [ $? -eq 1 ] ; then
        echo "${1} - failed test 0" results.txt
        echo "${1} - failed test 0" >> results.txt
    fi
}

# Tests the provision (ie runs the provisioner the 2nd time)
function provision_test {
    scripts/provision.bash
    if [ $? -eq 0 ] ; then
        echo "${1} - passed"
        echo "${1} - passed" >> results.txt
    else
        echo "${1} - failed"
        echo "${1} - failed" >> results.txt
    fi
}

# Prepare the testing environment
prepare

for i in "${TESTS[@]}"
do
    test_os $i
    run_test $i
    provision_test $i
    cleanup
done

# cleanup
rm -rf puppet/modules/*
rm -rf puppet/g10k_cache/*
rm *.log