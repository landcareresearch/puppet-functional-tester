#!/bin/bash
CACHEDIR="puppet/g10k_cache"
PUPPETFILE="../tests/Puppetfile"

# cleanup cache dir
if [ ! -d $CACHEDIR ] ; then
  mkdir $CACHEDIR
else
  if [ -d $CACHEDIR ] ; then
    rm -rf $CACHEDIR/*
  fi
fi

if [ -d puppet/modules ] ; then
  rm -rf puppet/modules
fi

if [ -d modules ] ; then
  rm -rf modules
fi

scripts/g10k -force -cachedir=$CACHEDIR -puppetfile -puppetfilelocation $PUPPETFILE 

mv modules puppet/.