#!/bin/bash

if [ ! -e /home/vagrant/.provision.txt ] ; then
    sudo rpm -Uvh https://yum.puppet.com/puppet7-release-el-8.noarch.rpm
    sudo yum install -y puppet-agent
    touch /home/vagrant/.provision.txt
fi
