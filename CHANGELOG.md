# Puppet Functional Tester Changelog

## 2022-04-08

- Added centos8 support.
- Installs Puppet 7 by default.
- Renamed centos configuration.

## 2022-01-10

- Added Ubuntu 21.04 and 21.10 support.
- Added a changelog.
