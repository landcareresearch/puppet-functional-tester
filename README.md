# Puppet Functional Tester

## About

This repository contains vagrant code for running functional testing on puppet modules.   This project uses [G10K](https://github.com/xorpaul/g10k) for dependency management.  The testing environment expects a file in the parent directory (tests/init.pp) to run the tests.

## Requirements

The system must have the following already installed.

* virtualbox
* vagrant
* puppet

## Setup

This repository will include the puppet/modules directory for the puppet module dependencies.  

### OS File

A os.bash file should be created in the parent/tests directory.

The file should contain the following format

```bash
#!/bin/bash
TESTS=(
"centos7.8"
"ubuntu16.04"
"ubuntu18.04"
"ubuntu20.04"
)
```

The names in the array correspond to the files in the config directory.  Add or subtract the operating systems you will to test.

### Puppetfile

Puppetfile is used for managing dependencies.  This expects a Puppetfile to be located in the parent/tests directory.

### Puppet Test File

A puppet file that contains puppet code that will be deployed to the VM is required.  This expects a init.pp file to be located in the parent/tests directory.

### Virtualbox

This testing environment uses virtualbox.  The IP address for the VM is '192.168.33.10'.  Make sure this IP is available when testing.

## Operating Systems

The vagrant configuration file uses a variable to load the OS used by the vagrant provider (such as virtual box).   This allows for a script to iterate through all supported OS's.

### Selecting an OS

If you want to test a specific OS Version, do the following.

* Edit the config/testos.rb
* Set the require statement to the OS to be tested.

See below:

```ruby
require './config/ubuntu20.04'
```

## Test Single VM

To test a single configuration do the following:

```bash
vagrant destroy
./scripts/run.bash
```

This will ensure that any previous test runs are destroyed before starting a new one.  See the output.  The last line should indicate if the test passed or failed.

## Test All VMs

To test all supported vms, do the following:

```bash
./test.bash
```

## Results

A file will be generated called results.txt

Each line of the file contains the results of the functional test for the specified operating system.

The format is as below:

```txt
[OS] - [passsed/failed]
```

The first parameter is the operating system tested.  The second parameter is the result of the funcational test.

* Passed - Means the puppet code was run sucessfully for the specified environment.
* Failed - Means the puppet code was run unsucessfully for the specified environment.  There was an error during the puppet apply.

### Result Example

Below is the results of a test for Ubuntu 16.04, 18.04, and 20.04.

```txt
ubuntu16.04 - passed
ubuntu18.04 - passed
ubuntu20.04 - passed
```
