# @summary Ubuntu 21.10's OS Configuration
# Note, puppet is not officially supported so must use Ubuntu 20.04's package which works
module MyVars
  OS     = 'ubuntu/impish64'.freeze
  PUPPET = 'scripts/puppet-install/ubuntu21.04.sh'.freeze
end
