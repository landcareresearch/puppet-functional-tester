# @summary Ubuntu 16.04's OS Configuration
module MyVars
  OS     = 'ubuntu/bionic64'.freeze
  PUPPET = 'scripts/puppet-install/ubuntu.sh'.freeze
end
