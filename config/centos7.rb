# @summary Centos 7's OS Configuration
module MyVars
  OS     = 'generic/centos7'.freeze
  PUPPET = 'scripts/puppet-install/centos7.sh'.freeze
end
