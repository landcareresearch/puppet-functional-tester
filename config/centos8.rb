# @summary Centos 8's OS Configuration
module MyVars
  OS     = 'generic/centos8'.freeze
  PUPPET = 'scripts/puppet-install/centos8.sh'.freeze
end
