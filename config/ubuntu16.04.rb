# @summary Ubuntu 16.04's OS Configuration
module MyVars
  OS     = 'ubuntu/xenial64'.freeze
  PUPPET = 'scripts/puppet-install/ubuntu.sh'.freeze
end
